from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand

from wafer.talks.models import Talk


FROM = 'online-cfp@debconf.org'
SUBJECT = 'Jitsi test calls for MiniDebConf Online'
BODY = '''\
Dear speaker,

If you haven't already done so, please come to #minidebconf-online-speakers on
the OFTC IRC network to arrange a test jitsi call ahead of your talk.  The test
call is an opportunity to check that audio and video are working well, and to
answer any questions you may have.

Those unfamiliar with IRC will probably find it easiest to connect via the
webchat at https://webchat.oftc.net/ - otherwise, join us by whichever way you
usually access IRC.

For any further assistance, you can find us at <online-cfp@debconf.org> or on
the above-mentioned IRC channel.

All the best,
the MDCO team
'''


class Command(BaseCommand):
    help = "Ask speakers to do test calls."

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),

    def notify(self, talk, dry_run):
        to = [user.email for user in talk.authors.all()]

        subject = SUBJECT
        body = BODY

        if dry_run:
            print('I would badger speakers of: %s' % talk.title)
            return
        else:
            print('Badgering speakers of: %s' % talk.title)
        email_message = EmailMultiAlternatives(
            subject, body, from_email=FROM, to=to)
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        for talk in Talk.objects.filter(status='A'):
            self.notify(talk, dry_run)
