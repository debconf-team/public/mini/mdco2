from django.core.management.base import BaseCommand
from wafer.schedule.models import ScheduleItem

import datetime
import os
import requests


class Command(BaseCommand):
    help = 'Load video URLs from sreview.debian.net (by ScheduleItem)'

    def add_arguments(self, parser):
        parser.add_argument(
            '--sreview-url', metavar='URL', type=str,
            default='https://sreview.debian.net/released.json',
            help='URL for the released.json file in the SReview instance',
        )

        current_year = datetime.datetime.today().year
        parser.add_argument(
            '--base-url', metavar='URL', type=str,
            default='https://meetings-archive.debian.net/pub/debian-meetings/%s' % current_year,
            help='Base URL for the videos released by sreview',
        )

        parser.add_argument(
            '--dry-run', action='store_true',
            help='Only show what would be done',
        )

    def handle(self, *args, **options):
        jsonurl = options['sreview_url']

        jsondata = requests.get(jsonurl)
        jsondata.raise_for_status()

        data = jsondata.json()

        baseurl = options['base_url']

        for entry in data['videos']:
            try:
                scheduleitem_id = int(entry['eventid'])
            except ValueError:
                continue
            schedlueitem = ScheduleItem.objects.get(pk=scheduleitem_id)
            talk = schedlueitem.talk
            url = entry['video'].lstrip('/')
            if url.endswith('.lq.webm'):
                url = url.rsplit('.', 2)[0] + '.webm'
            url = os.path.join(baseurl, url)
            if options['dry_run']:
                print('Would load video for <%s>: %s' % (talk, url))
            else:
                talk.urls.update_or_create(
                    description='Video',
                    defaults={"url": url},
                )
                print('Loaded video for <%s>: %s' % (talk, url))
